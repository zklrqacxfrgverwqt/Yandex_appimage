#!/bin/bash

# Verify the presence of the required packets
if ! command -v wget &>/dev/null || ! command -v ar &>/dev/null || ! command -v tar &>/dev/null || ! command -v sed &>/dev/null; then
    if command -v apt &>/dev/null; then
        echo "Please install the required packages:"
        echo "sudo apt install wget binutils tar sed -y"
    elif command -v pacman &>/dev/null; then
        echo "Please install the required packages:"
        echo "sudo pacman -S wget binutils tar sed --noconfirm"
    elif command -v yum &>/dev/null; then
        echo "Please install the required packages:"
        echo "sudo yum install wget binutils tar sed -y"
    else
        echo "Please install the packages wget binutils tar sed"
    fi

    exit 1
fi

APP=yandex-browser
mkdir tmp
cd ./tmp
wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage -O appimagetool
chmod +x ./appimagetool

#wget https://browser.yandex.ru/download/?banerid=6300000000&zih=1&beta=1&os=linux&x64=1&package=deb&full=1
wget --user-agent="Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36" https://browser.yandex.ru/download?banerid=6301000000\&os=linux\&package=deb\&x64=1 -O yandex.deb
ar x ./*.deb
tar xf ./data.tar.xz
mkdir $APP.AppDir
mv ./opt/yandex/browser/* ./$APP.AppDir/
mv ./usr/share/applications/*.desktop ./$APP.AppDir/
sed -i 's#Exec=/usr/bin/yandex-browser-stable#Exec=yandex-browser#g' ./$APP.AppDir/*.desktop
cp ./$APP.AppDir/*product_logo_16.png ./$APP.AppDir/$APP.png

tar xf ./control.tar.xz
VERSION=$(cat control | grep Version | cut -c 10-)

cat >> ./$APP.AppDir/AppRun << 'EOF'
#!/bin/sh
APP=yandex-browser
HERE="$(dirname "$(readlink -f "${0}")")"
export UNION_PRELOAD="${HERE}"
exec "${HERE}"/$APP "$@"
EOF
chmod +x ./$APP.AppDir/AppRun
ARCH=x86_64 ./appimagetool -n ./$APP.AppDir
cd ..
mv ./tmp/*AppImage ./yandex_browser_stable-$VERSION-x86_64.appimage
rm -Rfv ./tmp
echo "done"
