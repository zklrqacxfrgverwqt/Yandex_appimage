## About
Convert yandex.deb package to yandex.appimage

### Requirements
- On Debian, Kali, Ubuntu, Mint:
```bash
sudo apt install wget binutils tar sed -y
```

- On Arch, Manjaro:
```bash
sudo pacman -S wget binutils tar sed --noconfirm
```

- On Red Hat, CentOS, Fedora:
```bash
sudo yum install wget binutils tar sed -y
```

### How to use:
```bash
chmod +x yandex_stable.sh
```

```bash
./yandex_stable.sh
```

### Official links
[Yandex Search Engine](https://yandex.com/)
